// Create a react component
import React, {
    Component,
    AppRegistry,
    View,
    Text,
    StyleSheet
} from 'react-native';
import DayItem from './src/day-item.js';
//var DayItem = require('./src/day-item.js');
const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

class Weekdays extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text>
                    Days of the week:
                </Text>
                { DAYS.map((day) => {
                    return <DayItem key={day} day={day}/>
                }) }
            </View>
        );
    }
}

// Style the React component
const styles = StyleSheet.create({
    container: {
        flex: 1,    // height=100%, weight=100%
        justifyContent: 'center', // y direction
        alignItems: 'center' // x direction
    }

});

// Show the react component on the screen
AppRegistry.registerComponent('rn_weekdays', () => Weekdays);



///**
// * Sample React Native App
// * https://github.com/facebook/react-native
// */
//'use strict';
//import React, {
//    AppRegistry,
//    Component,
//    StyleSheet,
//    Text,
//    View
//} from 'react-native';
//
//class rn_weekdays extends Component {
//    render() {
//        return (
//            <View style={styles.container}>
//                <Text style={styles.welcome}>
//                    Welcome to React Native!
//                    This is our first App!
//                </Text>
//                <Text style={styles.instructions}>
//                    To get started, edit index.ios.js
//                </Text>
//                <Text style={styles.instructions}>
//                    Press Cmd+R to reload,{'\n'}
//                    Cmd+D or shake for dev menu
//                </Text>
//            </View>
//        );
//    }
//}
//
//const styles = StyleSheet.create({
//    container: {
//        flex: 1,
//        justifyContent: 'center',
//        alignItems: 'center',
//        backgroundColor: '#F5FCFF',
//    },
//    welcome: {
//        fontSize: 20,
//        textAlign: 'center',
//        margin: 10,
//    },
//    instructions: {
//        textAlign: 'center',
//        color: '#333333',
//        marginBottom: 5,
//    },
//});
//
//AppRegistry.registerComponent('rn_weekdays', () => rn_weekdays);
